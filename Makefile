# Makefile for the cstrings tool by Eric S. Raymond
#
# SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause

VERS=$(shell sed -n <NEWS.adoc '/^[0-9]/s/:.*//p' | head -1)

CFLAGS=-O
LDFLAGS=-s

# Stock lex works too, but gives a "Non-portable Character Class\" warning 
# and may choke on the %option yylineno in cstrings.l
LEX=flex

cstrings: cstrings.l
	$(LEX) cstrings.l
	mv lex.yy.c cstrings.c
	$(CC) $(CFLAGS) cstrings.c $(LDFLAGS) -o cstrings

# Note: to suppress the footers with timestamps being generated in HTML,
# we use "-a nofooter".
# To debug asciidoc problems, you may need to run "xmllint --nonet --noout --valid"
# on the intermediate XML that throws an error.
.SUFFIXES: .html .adoc .1

.adoc.1:
	asciidoctor -D. -a nofooter -b manpage $<
.adoc.html:
	asciidoctor -D. -a nofooter -a webfonts! $<

install: cstrings.1 uninstall
	cp cstrings /usr/bin
	cp cstrings.1 /usr/share/man/man1/cstrings.1

uninstall:
	rm -f /usr/bin/cstrings /usr/share/man/man1/cstrings.1

clean:
	rm -f lex.yy.c cstrings cstrings.c *~ cstrings.tar.gz *.html
	rm -f *.rpm cstrings-*.tar.gz cstrings.1

reflow:
	@clang-format --style="{IndentWidth: 8, UseTab: ForIndentation}" -i cstrings.l

SOURCES = README COPYING NEWS.adoc control cstrings.adoc Makefile cstrings.l 

cstrings-$(VERS).tar.gz: $(SOURCES) cstrings.1
	@ls $(SOURCES) cstrings.1 | sed s:^:cstrings-$(VERS)/: >MANIFEST
	@(cd ..; ln -s cstrings cstrings-$(VERS))
	(cd ..; tar -czvf cstrings/cstrings-$(VERS).tar.gz `cat cstrings/MANIFEST`)
	@(cd ..; rm cstrings-$(VERS))

dist: cstrings-$(VERS).tar.gz cstrings.html

release: cstrings-$(VERS).tar.gz cstrings.html
	shipper version=$(VERS) | sh -e -x

refresh: cstrings.html
	shipper -N -w version=$(VERS) | sh -e -x
